import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';

/**
 * Generated class for the SettingsPage page.
 *
 * See http://ionicframework.com/docs/components/#navigation for more info
 * on Ionic pages and navigation.
 */

@Component({
  selector: 'page-settings',
  templateUrl: 'settings.html',
})
export class SettingsPage {
  language:string;
  constructor(public storage: Storage,public navCtrl: NavController, public navParams: NavParams, public translate:TranslateService) {
    this.language = translate.currentLang;
  }

  
  onChange(e) {
    
  
  this.storage.get('language').then(data => {

      if(e=="en")
      {
            this.storage.set('language', "1");
            this.translate.setDefaultLang(e);
      }
      else{
          this.storage.set('language', "2");
            this.translate.setDefaultLang(e);

      }
      
    });
  }
}
