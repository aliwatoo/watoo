import { Component } from '@angular/core';
import { Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { TranslateService } from '@ngx-translate/core';
import { Storage } from '@ionic/storage';


import { HomePage } from '../pages/home/home';
@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage:any = HomePage;

  constructor(public storage: Storage,platform: Platform, statusBar: StatusBar, splashScreen: SplashScreen, translate: TranslateService) {
     
     this.storage.get('language').then(data => {
      let objFromString = JSON.parse(data);
      if (data !== null && data !== undefined) {
        console.log("helo");
        if(data =="1" )
        {
          translate.setDefaultLang('en');
        }
        else {
          translate.setDefaultLang('es');
         }
      } 
      else {
        this.storage.set('language', "1");
        translate.setDefaultLang('en');
      }
    });
     
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();
    });
  }
  
}

